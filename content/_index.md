&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Qu’est-ce qu’une culture symbiotique de bactéries et de levures (SCOBY) peut nous apprendre — à nous, humains — relativement à nos façons collaborer avec, prendre soin de, cultiver, exploiter, les autres vivants dont nous nous nourrissons ou à partir desquels nous fabriquons les objets de notre quotidien ? 

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pendant ce workshop à l’Université de Nîmes, nous suivons la kombucha comme une guide et abordons différents domaines : de l’alimentaire (gastronomique ou santé) au design textile, en passant par des pratiques de hacking. Nous étudions en quoi les cultures libres et les mondes de la fermentation entrent en résonance.

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Les différentes expérimentations menées (matière, teinture, gravure laser, tissage et maille, monitoring, conception d’éléments pour permettre les mises en culture, traitement et post-traitement…) sont documentées et référencées dans ce site. 

