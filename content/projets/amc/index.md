# AMC

(Placez le curseur sur les images pour faire apparaitre les légendes)

# **OBSERVATION MICROSCOPIQUE DE FERMENTATION : KOMBUCHA ET KEFIR**.

![La préparation](IMG_20230123_103425.jpg "La préparation")
![Les échantillons](IMG_20230123_104435.jpg "Les échantillons")

**Echantillon n°1** : 

![Cellulose de 1 an infectée](P1450827.JPG "Cellulose de 1 an infectée")
![Très gros plan sur l'infection](P1450825.JPG "Très gros plan")

Bout de moisissure contenu sur une cellulose de 1 an. Beaucoup de bactéries en bâtonnets simples, rares formes de filaments de champigon.
![Observation moisissure de Kombucha de 1 an](IMG_20230123_110840.jpg "Observation de moisissure de kombucha")

**Echantillon n°2** : Grain de kéfir de figue. Grande conscentration de levures à l’aspect gonflé à cause de l’eau (les levures se gorgent d'eau) Moins de bactéries.

![Amas de levures contenues dans un grain de kéfir](IMG_20230123_114412.jpg "Beaucoup de levure")

**Echantillon n°3** : Kéfir de figue. Cristaux d'origine inconnue accompagnés de quelques bactéries.

![Cristaux non indentifiés](IMG_20230123_120106.jpg "Cristaux de kéfir ou micro-fissures de la plaquette ?")

**Echantillon n°4** : Kombucha de 1 an (liquide). Moins de 10000 bactéries/cl, il y en a très peu. Presque vide au niveau des bactéries. Vie incertaine. Mais reste quelques levures.
![il y a peu d'organismes](IMG_20230123_113503.jpg "il y a peu d'organismes")

les microscopes
![Microscope grossissement x40 Max](IMG_20230123_144502.jpg "Grossissement x40 Max")
![Microscope grossissement x100 Max](IMG_20230123_144639.jpg "Grossissement x100 Max avec huile")

# **Mesure du pH des échantillons par pHmètre**

![Un pHmètre](IMG_20230123_151342.jpg "Un pHmètre")
Avant toute mesure, penser à calibrer l'appareil avec un liquide de pH neutre (7 pH) !

**Echantillon Kombucha de 1 an :** 2.21pH. 

pH proche de celui du citron (~2pH). Plus la kombucha est âgée, plus elle est acide. Mais l'acidité force l'arrêt du métabolisme, les levures entrent en dormance et se déposent sur le fond.

**Echantillon kéfir de figue :** 3.81pH. Il est donc moins acide que la kombucha.


# **OBSERVATION TAMBOUR** :


**PB** : A partir des instruments de musique de l’année dernière, comment optimiser leur efficacité et leur durée.  
Comment pouvons-nous, à partir d’anciens modèles comprendre et optimiser la « peau de kombucha » des percussions et ces autres matériaux.  
Nous avons 4 prototypes de percussion en kombucha datant de l’année 2022. Nous essayons de trouver une solution au problème présenté.

![Un instrument de kombucha](P1450918.JPG "Un instrument fait à partir de cellulose de kombucha")

**Au niveau du son** : sur les 4, nous en avons 1 qui résonne assez bien avec un bruit grave ; Le deuxième résonne d'un son aigüe mais faible car la cellulose de kombucha utilisée est trop fine; 
Le troisième est un tambourin à 2 faces (hochet tambourin ou taogu) qui possède une face déchirée (n’a pas supporté le choc des perles/boules). Le quatrième n'est pas concluant car la cellulose de kombucha est trop épais.
La solution serait de faire sécher directement la cellulose (pas trop épaisse ni trop fine) sur le support, de préférence en bois pour une meilleur résonnance. Tel un moulage, le kombucha imprégnera la forme du support. 
Il faut impérativement le traiter pour qu’il ne se transforme pas en "chips".
Les traitements possibles sont à base d’huile de lin, cire d’abeille et térébenthine (aussi utiliser pour le traitement du bois). Ou un traitement à la glycérine.



# **MISE EN CULTURE** : kombucha et café

Test de mise en culture de kombucha au café.  
L’expérimentation vise à savoir si la cellulose de kombucha peut se développer dans milieu mixte à base de thé et de café.

Ingrédients :

25cl de café

50cl de starter (kombucha)

Cellulose (kombucha)

1 sucre

![Ingrédients](IMG_20230124_143117.jpg "Ingrédients")

En premier temps, produire au moins 25cl de café puis ajouter le sucre. Laisser refroidir.  
Une fois le café froid, ajouter les 50cl de starter et un bout de cellulose de celui ci.  
Couvrir avec un tissus afin de protéger la culture tout en la laissant respirer.  
Poser la culture dans une chambre de fermentation chauffée à bonne température et laisser reposer. 


![Préparation](IMG_20230124_145552.jpg "Préparation")
![Mise en pot](IMG_20230124_150506.jpg "Mise en pot")
![Dans la chmabre de fermentation](IMG_20230124_151023.jpg "Dans la chambre de fermentation")





# **OBSERVATION DE LA CHLORELLE**

![Mise en culture de la chlorelle](IMG_20230124_144401.jpg "Mise en culture de la chlorelle")

La chlorelle est une micro-algue qui, grâce à la photosynthèse, produit de l’oxygène en journée et respire la nuit.  
La photosynthèse est possible grâce à ses pigments vert appelé la chlorophylle présente dans les chloroplastes des cellules du végétaux.  
Nous avons observé cette algue au microscope, dont voici quelques images :

![préparation échantillon chlorelle 1](IMG_20230124_151956.jpg "Préparation de l'échantillon de chlorelle")

![préparation échantillon chlorelle 2](IMG_20230124_152018.jpg "Préparation de l'échantillon de chlorelle")

![observation chlorelle 1](IMG_20230124_153214.jpg "Observation chlorelle 1")

![observation chlorelle 2](IMG_20230124_155705.jpg "Observation chlorelle 2")

![observation chlorelle 3](IMG_20230124_154340.jpg "Obeservation chlorelle 3")

![observtaion chlorelle 4](IMG_20230124_154215.jpg "Observation chlorelle 4")

# **Schéma V2**

schéma sur le processus de fermentation de la kombucha.

# **Production finale : Fanzine**

Fansine à propos de nos observations au microscope des échantillons de kombucha, kéfir et chlorelle.

-8 pages + poster  
-Impression risographie  
-Reliure avec agrafe

![wip de nôtre fanzine](Capture_d'écran_du_fanzine.PNG "wip du fanzine")