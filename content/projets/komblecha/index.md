## AVQ


![logo](logo.jpg "logo")

## **Protocole Kombluecha**

Le protocole Kombluecha est un protocole de teinture naturelle de kombucha fraîche et sèche grâce a l’indigo naturel présent dans le pastel du Languedoc.
Une fiche explicative sur la plante, son histoire, et son fonctionnement, faite par nos soins, est disponible ici en pdf :

[Fiche explicative du pastel](guede.pdf)

La teinture au pastel naturel s’effectue en suivant des étapes précises et son efficacité dépend de la saison de récolte des feuilles. Le but de la réaction est d’obtenir de l’indigotine, colorant bleu, à partir des molécules appelées des « précurseurs ». Avant toute tentative, les feuilles doivent être fraîchement récoltées, durant leur première année, et lavées de toutes impuretés.

![schema](schema.jpg "schema")

Nous avons donc effectué 15 tests sur 3 types d’échantillons différents : de la kombucha fraiche, sèche, et du tissu blanc. Ci-dessous le tableau des expériences effectuées.  

Lors de la réalisation des tests, nous nous sommes rendu compte que le pastel cueilli n’était pas efficace car ce n’est tout simplement pas la saison ; seuls les tests 1 et 2 sont à partir de pastel. Les tests 4, 5 et 6 sont à la poudre d’indigo pour simuler un résultat coloré de saison (le test 3, similaire au 2, est omis).
Les 4 premiers tests ont été réduits à l’hydrosulfite de sodium, alors que le 5ème (test 6) grâce aux bactéries et au sucre d’une banane trop mûre. 

### ***Étapes des tests 1 et 2 :***

1-Lavage des feuilles

![lavage](lavage.jpg "lavage")

2-Faire bouillir de l’eau 

3-Ajouter des feuilles jusqu’au niveau de l’eau (l’eau doit couvrir toutes les feuilles)

4-Faire infuser et bien mélanger pendant 10 minutes

![cuisson](cuisson.jpeg "cuisson")

5-Mettre de coté et laisser agir 15 minutes en veillant que le bain ne descende pas en dessous de 50° à partir de cette étape

![Temperature](Temperature.jpeg "temperature")

6-Battre la solution afin d’y faire rentrer de l’air jusqu’à obtention d’une mousse bleue puis blanche

![mousse](mousse.jpg "Mousse")

7-Ajouter la Chaux et l’Hydrosulfite de sodium et mélanger sans faire rentrer d’air dans le bain

![Chaux](Chaux.jpeg "Chaux")

![Hydrosulfite](Hydrosulfite.jpeg "Hydrosulfite")

![MelangeHydro](MelangeHydro.jpeg "MelangeHydro")

8-Tester le ph et ajuster si besoin, le bain doit être basique, environ ph10, à partir de cette étape

![pH](pH.jpeg "pH")

10-Tremper l’échantillon pendant 3 minutes sans qu’il ne touche de trop la mousse et les parois

![Trempage](Trempage.jpeg "trempage")

11-Sortir l’échantillon et l’oxygéner au maximum en le faisant sécher à l’air libre en suspension quelques minutes

12-Laisser sécher jusqu’à disparition totale d’humidité

![sechage](sechage.jpg "sechage")

### ***Étapes des test 4 et 5 :***

1-Faire bouillir de l’eau

2-Ajouter l’indigo en poudre et bien mélanger même s’il n’est pas soluble

![indigo](indigo.jpeg "indigo")

3-Veiller à ce que le bain ne descende pas en dessous de 50° à partir de cette étape

4-Battre la solution afin d’y faire rentrer de l’air jusqu’à obtention d’une mousse bleue puis blanche

5-Ajouter la Chaux et l’Hydrosulfite de sodium et mélanger sans faire rentrer d’air dans le bain

6-Tester le ph et ajuster si besoin, le bain doit être basique, environ ph10, à partir de cette étape

7-Tremper l’échantillon pendant 3 minutes sans qu’il ne touche de trop la mousse et les parois

8-Sortir l’échantillon et l’oxygéner au maximum en le faisant sécher à l’air libre en suspension quelques minutes

9-Laisser sécher jusqu’à disparition totale d’humidité

Le test 6 est exactement le même en termes d’étapes que le 4 et le 5 mais l’hydrosulfite est remplacée par de la banane trop mûre.

![banane](banane.jpeg "banane")

Pour réaliser ces tests, nous avons suivi une feuille de route que nous avons créé, appelée « Grimoire de documentation de la Kombucha teintée ».
Voici un lien pour la récupérer en pdf :

[Grimoire de la documentation de la kombucha teintée](documentation.pdf)

Les résultats de nos tests et les données respectives sont compilés dans ce tableau :

{{< get-csv sep="," url="static/tables/pastel.csv" >}}

### ***Résultats et interprétations :***

La poudre d’indigo utilisée est en réalité une simulation de ce que pourrait produire des feuilles de Pastel d’un an et de saison. Selon des témoignages et des résultats d’expériences sur des forum, le résultat sur tissu est presque équivalent, que la teinture soit naturelle (en été et les plantes ayant 1an), ou qu’elle soit faite à partir de poudre d’indigo. 
On peut donc en déduire qu’il en est de même pour la kombucha, si les conditions de production d’enzymes et de précurseurs sont réunies au sein des plantes, le résultat sera le même que pour la poudre d’indigo.
La bonne recette est donc celle-ci-dessous, en partant sur un minimum de 2 trempages pour une répartition équilibrée.

[Recette teinture naturelle](recette.pdf)

Il est également possible de faire, sur de la kombucha, des techniques de colorations qui marchent sur le tissu, comme le tye-and-dye, le shibori, ou d’autres motifs aléatoires et répétitifs. 

![tyeanddye](tyeanddye.jpeg "tyeanddye")

Cette solution naturelle pour teindre de la kombucha est donc bien trouvée. S’inscrivant en plein dans l’atmosphère DIY du workshop, elle est donc particulièrement adaptée pour prolonger des recherches étiques sur des productions naturelles et saines comme celles effectuées depuis 2 ans à Unîmes, ou pour une production maison/écologique. 
Étant un coproduit naturel, la kombucha trouve de plus en plus sa place dans notre société, alors il est important de continuer sur cette lancée écologique et transformer ce produit de la façon la plus propre possible, à la manière d’une teinture naturelle par exemple.


