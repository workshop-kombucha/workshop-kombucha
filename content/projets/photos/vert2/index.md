# Série Photo N°10

![ Picture](1vert2.webp "1vert2")  

.  

#### Culture 1  
(15/10/2022 au 28/11/2022)  
Teinture au colorant alimentaire bleu ciel E131 hydrosoluble  
Sans traitement  
Fond blanc, lumière blanche, tablette lumineuse, lumière bleu et rouge  
Matière sèche et souple  

.  

![ Picture](2vert2.webp "2vert2")  

![ Picture](3vert2.webp "3vert2")  

![ Picture](4vert2.webp "4vert2")  
