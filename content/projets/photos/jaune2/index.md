# Série Photo N°5

![ Picture](1jaune2.webp "1jaune2")  

.  

#### Culture -2  
(Workshop 01/2022)  
Cellulose pliée sur elle-même  
Sans traitement  
Fond blanc, tablette lumineuse, feuille rose  
Matière fine, très souple, lisse, et douce  

.  

![ Picture](2jaune2.webp "2jaune2")  

![ Picture](3jaune2.webp "3jaune2")  

![ Picture](4jaune2.webp "4jaune2") 
