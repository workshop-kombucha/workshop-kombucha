# Série Photo N°11

![ Picture](1vert3.webp "1vert3")  

.  

#### Culture 1  
(15/10/2022 au 28/11/2022)  
Après séchage  
Teinture au colorant alimentaire bleu ciel E131 hydrosoluble  
Fond blanc, lumière blanche, tablette lumineuse, lumière verte, orange et rouge  
Matière sèche au centre, très sèche et friable sur les bords. Beaucoup d’aspérités  

.  

![ Picture](2vert3.webp "2vert3")  

![ Picture](3vert3.webp "3vert3")  

![ Picture](4vert3.webp "4vert3")  
 