# Série Photo N°9

![ Picture](1bleu.webp "1bleu")  

.  

#### Culture 1, Échantillon 7  
(15/10/2022 au 28/11/2022)  
Fin d’égouttage, après teinture  
Teinture au colorant alimentaire bleu ciel E131 hydrosoluble  
Fond blanc, lumière blanche, tablette lumineuse  
Matière sèche et fine  

.  

![ Picture](2bleu.webp "2bleu")  

![ Picture](3bleu.webp "3bleu")  

![ Picture](4bleu.webp "4bleu")  


