# Documentation photo
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Les celluloses de kombucha ont été séchées, pour certaines colorées et traitées, elles sont prêtes à être photographiées.

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Notre travail est concentré sur le gros plan et le détail. Nous avons joué avec la transparence grâce à une table lumineuse, les reliefs sont mis en avant avec les éclairages et de nouvelles nuances sont apportées à l’aide de lumières colorées. Chaque particularité a été mise en évidence et capturée pour obtenir des clichés uniques et singuliers.

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ces celluloses sont parfois très visqueuses, gélatineuses, ou encore très sèches. Mais une fois passées sous l’objectif, elles révèlent leur potentiel. Leur finesse et leurs couleurs vives sont pour certaines comparables à un vitrail ainsi avec la lumière elles laissent place à de jolis motifs, nuances et détails. Ce qui n’était qu’une cellulose de kombucha devient alors un paysage.

{{< grille >}}

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Toutes ces expérimentations photos, permettent de mettre en avant la polyvalence esthétique de la kombucha. Ce biomatériau prend de nombreuses formes, que ce soit dans la couleur ou la texture.
Outre sa plasticité, l’hétérogénéité de ce matériau peut nous conduire à penser, qu’il va être activement utilisé dans le design de produit ainsi que tous les domaines impliquant l’utilisation de matériaux.

