# Série Photo N°8

![ Picture](1violet.webp "1violet")  

.  

#### Culture 2 Échantillon 16  
(5/12/2022 au 18/01/2023)  
Fin d’égouttage, après teinture  
Teinture au colorant alimentaire bleu ciel E131 hydrosoluble et rouge E122  
Table lumineuse, fond blanc  
Matière avec le centre sec et fin, tour épais, humide et gélatineux

.  
 
![ Picture](2violet.webp "2violet")  

![ Picture](3violet.webp "3violet")  

![ Picture](4violet.webp "4violet")   