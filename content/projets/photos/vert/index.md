# Série Photo N°1

![ Picture](1vert.webp "1vert")

.  

#### Culture 2 Échantillon 5  
(5/12/2022 au 18/01/2023)  
Fin de 2 jours de trempage puis séchage de 2 jours.  
Teinture au colorant alimentaire bleu ciel E131 hydrosoluble  
Fond blanc, tablette lumineuse, et feuille rose  
Matière sèche, grain peu prononcé, irrégulier avec des légers plis.
  
.  

![ Picture](2vert.webp "2vert")  

![ Picture](3vert.webp "3vert")  

![ Picture](4vert.webp "4vert")



