# Série Photo N°4

![ Picture](1rouge.webp "1rouge")  

.  

#### Culture 2 Échantillon 8  
(5/12/2022 au 18/01/2023)  
Fin de 2 jours de trempage puis séchage de 2 jours  
Teinture rose framboise, E122 hydrosoluble  
Fond blanc, tablette lumineuse, lumière verte  
Matière sèche, friable et humide selon les endroits  

.  

![ Picture](2rouge.webp "2rouge")  

![ Picture](3rouge.webp "3rouge")  

![ Picture](4rouge.webp "4rouge")  