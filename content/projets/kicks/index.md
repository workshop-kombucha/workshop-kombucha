# Recontextualisation :

Durant ce workshop mettant en relation élève, prof et intervenant nous avons étudier la mise en culture de la kombucha, ces propriétés, et sa mise en place. 
La kombucha est le résultat d’une réaction chimique à base de thé et de sucre qui contient bactérie et levure. 
Au cours de cette réaction plusieurs interactions se produisent qui vont donner naissance à une cellulose bactérienne. 
Une mise en culture nécessite donc des outils permettant de mesurer les propriétés qui vont permettre de témoigner du bon déroulement de celle-ci,
à savoir, la température, l’oxygène, et le ph. Notre travail consiste donc à mettre en place ces outils numériques. 

![culture de kombucha](51873438086_49b6e09be5_b.jpg)

# Fiche pratique : 

### EspHome : 
ESPHome est un système pour contrôler votre ESP32 par des fichiers de configuration simples mais puissants et les contrôler à distance via des systèmes domotiques.

![EspHome](capture1.jpg)

### Yaml : 
YAML est un langage de sérialisation des données qui est souvent utilisé pour coder des fichiers de configuration.

![yaml](capture2.jpg)

### Plaque de prototypage : 
Une platine de prototypage (appelée en anglais breadboard) est un dispositif qui permet de réaliser le prototype d'un circuit électronique et de le tester.

![plaque de prototypage](IMG_2415.jpg)

### Microcontrôleur (ESP32) : 
ESP32 est un microcontrôleur de type système sur une puce (SoC) d'Espressif Systems, qui intègre intégrant la gestion du Wi-Fi et du Bluetooth. 
Il permet de contrôler et joue le rôle d’intermédiaire entre les différents éléments électronique. 

![Microcontrolleur](IMG_2417.jpg)

### Tmp 117 : 
Le dispositif TMP117 de Texas Instruments est un capteur de température numérique haute précision.

![tmp117](tmp117.jpg)

### Capteur d’oxygène : 
Le capteur d'oxygène Gravity peut mesurer la concentration ambiante d'O2 avec précision et pratique.

![capteur d'oxygene](IMG_2357.jpg)

### Pt100 : 
Une sonde Pt100 est un type de capteurs de température aussi appelé RTD (détecteur de température à résistance) qui est fabriqué à partir de platine, 
et qui permet de mesurer la température dans un liquide. 

![pt100](PRT_12in_9in_vFS.jpg)

### Resistance : 
En électricité, le terme résistance désigne : une propriété physique : 
l'aptitude d'un matériau conducteur à s'opposer au passage d'un courant électrique sous une tension électrique donnée.
 
![resistance](resistance-470-ohms-anti-courant-residuel.jpg)

### Ph mètre : 
est une machine permettant de calculer le PH d’une solution.

![ph metre](03580901_pc_tle_c01-036-i0201.jpg) 

### Interface I2c :
 C'est un protocole de communication développé par Philips Semiconducteurs pour le transfert de données entre un processeur central et plusieurs esclave 
(capteurs, encodeurs etc…).


# Mise en culture et mesure de la température :
 
Durant la mise en culture de la kombucha, les bacs récupérés sont placés dans une chambre de fermentation, qui isole la kombucha de l’environnement extérieur. 
La kombucha nécessite une température entre 25 et 30 °C pour se développer comme il faut. En dessous de 22° la Kombucha n’est plus protégée, tandis qu’au-dessus de 30, 
il y a risque de tuer les levures et les bactéries. La chambre de fermentation nécessite donc des capteurs de température qui vont nous indiquer ces données, 
nous assurant ainsi la sécurité et la longue durée de vie du système.
Le TMP117 est un capteur de température numérique de haute précision. Le TMP117 dispose d'une interface compatible I2C, d'une fonctionnalité d'alerte programmable, 
et l'appareil peut prendre en charge jusqu'à quatre appareils sur un seul bus 
(Un bus informatique est un dispositif qui permet de transférer des données entre plusieurs appareils venant du même système numérique). 
Il est nécessaire lors de la mise en culture d’installer un capteur nous donnant la température intérieure, et un deuxième capteur nous donnant la température extérieure.
 

## Mise en place :

Dans un premier temps, nous commençons par récupérer une plaque de prototypage, auquel on ajoute un microcontrôleur. 
Le microcontrôleur possède plusieurs entrées permettant de connecter des périphériques extérieurs, à savoir, notre capteur de température. 
Le câble utilisé pour faire l’intermédiaire entre le capteur et le microcontrôleur se divise en 4 branchements. Rouge, noir, bleu et jaune. 
Le câble rouge se branche sur 3v3, à savoir l’alimentation. Le noir sur ground (la terre). Il permet d’éviter les problèmes de surtension.  
Le bleu et le jaune sont des câbles de transmission de données branchés à l'entrée d23 d22. 
Pour installer le deuxième capteur il suffit de les relier entre eux à l’aide de câbles de connexion. Chaque capteur possède une adresse qui va être renseigné sur yaml. 
L’adresse par défaut est 0x48. Lors de l’installation d’un deuxième capteur, il est nécessaire de renseigner une adresse différente. 
On coupe alors la connexion sur une adresse et on ajoute une connexion par un point de soudure sur l’autre adresse.

![mise en place temperature](IMG_2376.jpg)

 Une fois ce montage réalisé, on connecte l’ensemble a l’ordinateur via l’entrée USB. La mise en route du capteur nécessite un codage, 
indiquant à l’appareil les fonctions qu’il doit remplir. ESP home est l’interface sensé accueillir cette programmation. 
Une fois sur le site, cliquer sur l’édit correspondant à votre microcontrôleur. Puis y entrer le code de lancement suivant : 

Capteur de température :
```yaml
i2c:
  sda: 22
  scl: 23

sensor: 
  - platform: tmp117
    name: "living room temperature"
    update_interval: 10s
    address: 0x49
```


Deux Capteurs de température:
```yaml
i2c:
  sda: 22
  scl: 23

sensor: 
  - platform: tmp117
    name: "living room temperature"
    update_interval: 10s
    address: 0x49
  - platform: tmp117
    name: "2nd living room temperature"
    update_interval: 10s
    address: 0x48
```

La ligne I2C nous dit ou est ce qu’on veut que le bus I2C soit placé. La ligne de donnée est 22 et la ligne de cadenassage est sur le 23. 
On indique l’insertion du/des capteur/s. On indique donc le capteur, le nom, le temp de réponse, et l’adresse du capteur. 
Une fois le code entré, cliquer sur Install, et récupérer les données. 
Mise en culture et mesure de la température dans le liquide : 

Lors de la mise en place des bacs, au-delà de la température environnementale, la température du milieu est une information primordiale. 
On va donc venir placer un capteur qui puisse nous donner la température directement à l’intérieur du liquide. 
La Pt100 est une sonde résistive (RTD). Le code ci-dessous est la façon dont vous pouvez accéder à la température et l'exposer en tant que capteur. 
Les valeurs de filtre sont extraites de la fiche technique RP2040 pour calculer la tension en degrés Celsius. 
Mise en place : 
Ici, nous allons utiliser une résistance, un microcontrôleur, une plaque de prototypage, et un capteur de température. Le capteur de température possède deux sorties. 
L’une des sorties, est directement reliée à la fonction Ground du microcontrôleur, tandis que l’autre est reliée à la plaque de prototypage. 
Sur la même ligne d’horizon est branchée la résistance, le branchement 3V3, et VP.

![capteur de temperature liquide](IMG_0367.jpg)

Une fois le montage réalisé, on édit le capteur dans Esp Home et on rentre le programme suivant :
```yaml
sensor: 
  - platform: adc
    pin: GPIO36
    attenuation: auto
    name: "Température du milieu"
    update_interval: 10s
    unit_of_measurement: "°C"
    filters:
      - lambda: return ((560*1/(3.3/x-1))/100-1)/0.00385f;
```

La ligne adc (analogue to digital Converter) sert à calculer la tension. Le GPI036 nous indiques le Pin de connexion. 
S’en suis le nom, le temps de réponse, l’unité désiré, et la formule nous donnant la température. Il est nécessaire de les convertir en ° C car la mesure est en Volt : 
Pour calculer la température, il faut d’abord calculer la résistance à l’aide de cette formule : 
R=R’*1/(Vin/Vout-1)
R’= la resistance= 560 Ohm
Vin= alimentation = 3,3 V
Vout= mesure de l’ADC = x
Une fois qu’on a trouvé la valeur de la résistance on peut passer au calcul de la température :
T= ((R/R0) – 1) / lpd
R= résistance de la sonde de température
R0= resitance de la tension de température à 0°C = 100 Ohm
Lpd = coefficiant de temperature = 0,00385
Après avoir fait ce calcul on a la mesure de la température.


# Mise en culture et mesure de l’oxygène :
 
La mise en culture de la kombucha nécessite de l’oxygène. L’oxygène permet aux bactéries et à la levure de se développer plus rapidement afin de créer la cellulose bactérienne. 
Il a donc été envisagé de placer un élément extérieur permettant de combler ce besoin en oxygène et de renouveler l’air. 
L’élément permettant de répondre à cette contrainte est le bac de chlorelle. 
La chlorelle est une microalgue d’eau douce permettant de désintoxiquer la solution et d’apporter de l’oxygène.


## Mise en place du bac de chlorelle : 

Pour préparer la chlorelle, 2 L d’eau sont versé dans un contenant. Par la suite plusieurs éléments chimiques sont ajoutés : 
25 mg/l de CaCL2 (calcium chlorure dihydrate), 25 mg/l de NaCL (sodium hydrogénocarbonate), 250 mg/l de NH4CL (Ammonium chloride), 75 mg/l de MgSO4 (magnésium sulfate heptahydrate) 
et 150 mg/l de KH 2 PO4 (Potassium dihyydrogenophosphate). 

![mise en place bac de chlorelle1](IMG_2629.jpg)

![mise en place bac de chlorelle2](IMG_2628.jpg)

Le mélange nécessite un PH de 7.5, donc une solution légèrement basique. Nous avons alors procédé à l’étalonnage du PH mètre. 
Une fois le ph mètre installé, on rince la sonde avec de l’eau distillé et on calibre le ph mètre avec une solution acide et basique. 
On la rince, et on la plonge dans la solution. Une fois toute ces étapes faites, on verse une souche de chlorelle dans le liquide. Et on le laisse reposer.
  
![bac de chlorelle en repos](IMG_2393.jpg)


## Mise en culture et mesure de l’oxygène : 

Le capteur d'oxygène Gravity : I2C est basé sur des principes électrochimiques et peut mesurer la concentration ambiante d'O2 avec précision et pratique. 
Compatible Arduino, il peut être largement appliqué dans des domaines tels que les appareils portables, les dispositifs de surveillance de la qualité de l'air et les industries, 
les mines, les entrepôts et autres espaces où l'air n'est pas facile à circuler. Dans ce cas précis les capteurs d’oxygène sont placés à l’intérieur et à l’extérieur du bac de chlorelle.

 
## Mise en place :
 
Dans un premier temps, nous commençons par récupérer une plaque de prototypage, auquel on ajoute un microcontrôleur. 
Sur une plaque de prototypage, les lignes horizontales, sont interconnectées. Ces lignes seront donc utilisées pour connecter les 2 capteurs d’oxygène entre eux. 
Le système de branchage au microcontrôleur est le même que pour le capteur de température. 
Or, les câbles sont implantés dans la plaque de prototypage afin de connecter le deuxième capteur d’oxygène sur une seule et même ligne. 
Pour l’oxygen sensor on a les adresses 0X72 et 0X73. Ici on change l’adresse par des boutons contrairement au capteur de température. On a les boutons A0 et A1.
-0X72, A0=0, A1=1
-0X73, A0=1, A1=1
Lorsque l’on veut brancher un capteur on met A1, et quand on veut brancher les 2 on met A0. Une fois ce branchement réalisé, on place un capteur à l’extérieur, 
et un a l’intérieur du contenant, sans qu’il touche au liquide. On connecte ensuite le microcontrôleur a un ordinateur, puis on édit les capteurs à l’aide de Esp Home. 

![oxygene](IMG_0313.jpg)

![oxygene](IMG_0358.jpg)

On y entre le code de démarrage suivant :

```yaml 
#include "DFRobot_OxygenSensor.h"
#include "esphome.h"

#define COLLECT_NUMBER    10             // collect number, the collection range is 1-100.
class OxygenSensor : public PollingComponent, public Sensor {
 public:
  DFRobot_OxygenSensor Oxygen;
  int addr;
  // constructor
  OxygenSensor(int i2c_addr) : PollingComponent(15000) { addr = i2c_addr; }

  float get_setup_priority() const override { return esphome::setup_priority::BUS; }

  void setup() override {
	ESP_LOGI("oxygen", "Connecting to DFRobot OxygenSensor");
	int tries = 0;
	while(!Oxygen.begin(addr) && tries < 10) {
	  tries += 1;
          delay(1000);
	}
	if(tries == 10) ESP_LOGE("oxygen", "Failed to connect");
  }

  void update() override {
    float oxygenData = Oxygen.getOxygenData(COLLECT_NUMBER);
    publish_state(oxygenData);
  }
};
```

Include, nous indique que l’on inclue 2 codes différents à savoir "DFRobot_OxygenSensor.h" "esphome.h"
Esp logue I (infos) nous permet d’enregistrer les lignes dans les informations que l’on reçoit. 
Define collecte number correspond au nombre de connexion qu’il doit effectuer. 
Class correspond à la création d’une nouvelle classe qui est une sous-classe de public sensor dans laquelle on indique la récupération de données toutes les 15 secondes. 
While signifie tant qu’il ne détecte pas de capteur, alors il doit continuer à chercher. 

Une fois le code entré, cliquer sur Install, et récupérer les données. 

![graphique](ox.jpg)




