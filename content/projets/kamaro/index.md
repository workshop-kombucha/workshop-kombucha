# KAMARO
## Amélioration des housses de la chambre de fermentation.

![housse indesign](IMG_0346.jpg "housse indesign")

**INTRODUCTION :**
Pour avoir une bonne cellulose de kombucha, il faut respecter des critères. 
Si l’on ne suit pas ceux ci à la lettre, la kombucha risque soit de ne pas prendre, 
soit d’être contaminée, soit d’être abîmée par des insectes. 

**CONTRAINTES :**

**LA REPRODUCTIBILITE :**
Étant donné que la housse est un prototype qui devra être reproduit plusieurs fois, 
il faut penser à faire quelque chose qui se reproduit facilement, rapidement et à moindre coûts.
Il est donc possible d’avoir accès au patron avec les plans ainsi que leurs dimensions. (Tuto de la housse finale disponibble à la fin)

**LA RESISTANCE :**
Le bac remplis de kombucha sera amené à constamment glisser sur les glissières de l'échelle. 
Pour notre prototype de housse, il sera nécessaire d’utiliser des matériaux résistants pour ne pas que le tissu se déchire. 
De plus, ce dernier doit pouvoir résister à un lavage à 60°, permettant de le rendre propre de sorte à ce qu’il n’attire pas 
toutes sortes d’envahisseurs.

**LES INSECTES :**
Nous avons besoin d’une housse qui ne laissera passer ni les bactéries, 
ni les insectes et qui puisse s’ouvrir et se fermer facilement et rapidement. 

**LES CONTAMINATIONS :**
Il faut faire attention, lorsqu’on ouvrira la housse, 
à ce que le tissu ne touche pas le liquide, 
sinon il se gorgera de celui-ci et le sucre attirera les levures qui viendront créer des contaminations. 

**LA CHALEUR :**
Pour que le kombucha se développe correctement, il nécessite une température supérieure à 24 degrés, 
il faudra donc penser à une place pour inclure un tapis chauffant, celui-ci devra être facile à mettre et à retirer. 
Or, la housse reste la même été comme hiver, il suffit uniquement de ne pas mettre le tapis en période de forte chaleur.

**LA TAILLE :**
La housse doit être bien taillée au niveau de la largeur pour que le tissu plus épais réservé aux glissières soit ajusté. 
Mais au niveau de la longueur, le tissu devra être plus long, afin de n’avoir aucuns problèmes avec la fermeture éclair.
En effet, notre parti pris est de plus tendre la largeur, de manière à être sûr que le tissu ne touche pas l’eau.

**LES TISSUS :**
Il faut un tissu filtrant, 
pour que l’oxygène dont a besoin la kombucha puisse passer sans que les bactéries et surtout les levures ne se faufilent avec.

**DIMENSIONS BACS :**
39,6 cm largeur totale
26 cm largeur intérieure
41 cm longueur intérieure
52,5 cm longueur totale
6,8 cm hauteur totale
6 cm hauteur intérieure


**IDEE 1 :**

Création d’une housse rigide comprenant sur sa partie supérieure une plaque de fer séparée en deux en son centre, glissée dans du tissu. 
Lorsque l’on va ouvrir la housse, pour la coincer, on aura juste à poser la plaque sur l’autre et grâce à la rigidité ça ne pendra pas, 
et donc ça ne touchera pas le liquide.
Or après réflexion, l’humidité présente dans la chambre de fermentation viendra faire rouiller le fer, risquerait de salir le tissu et de contaminer la solution. .
De plus, cette idée ne présente pas un réel côté pratique sur plusieurs points; l’ouverture, 
le fait que cela ne pende pas réellement dans la solution, le poids (avant et après remplissage) etc.


**IDÉE 2 :**

Pour la fermeture éclair, à la place d’avoir une longue fermeture éclair qui prend tout l’avant, on va en utiliser deux : 

![la double fermeture éclair](IMG_0324.jpg "la double fermeture éclair")

- les deux fermetures éclairs seront ouvertes lorsqu’on rentrera le bac dans la housse

![les deux fermetures ouvertes](IMG_0325.jpg "les deux fermetures ouvertes")

- seule l’une des deux fermetures éclair sera ouverte, lorsque les bacs seront remplis et que l’on voudra les ouvrir 
pour ajouter de l’eau ou du starter. 

![seulement une fermeture est ouverte](IMG_0326.jpg "seulement une fermeture est ouverte")

L’un des côtés, sera relié par des cordelettes élastiques, qui viendront tirer sur le tissu et le rabattre sur le dessus, 
pour qu’il ne tombe pas dans le liquide. 
Ces élastiques seront placés de sorte à exercer la tension sur la zone de l’angle servant à l’ouverture.

![la tension grâce à l'élastique](IMG_0327.jpg "la tension grâce à l'élastique")


**IDÉE 3 :**

Élastiques qui rabattent une zone au lieu de plusieurs points séparés

Sur le devant de la housse, proche de l’ouverture, nous allons enfiler des renforts en plastique rigide, 
auxquels les élastiques seront attachés.

![renforts en plastique](IMG_0333.jpg "renforts en plastique")

Lorsque l’on va fermer la housse, les élastiques seront tendus au maximum, et lorsqu'on va ouvrir la fermeture éclair en grand, 
les cordelettes élastiques vont se détendre et ramener le rabat vers le dessus de la housse. 

![housse fermée avec les élastiques qui tendent](IMG_0330.jpg "housse fermée avec les élastiques qui tendent")

![housse ouverte avec les élastiques détendus](IMG_0331.jpg "housse ouverte avec les élastiques détendus")


Avoir tous les élastiques qui tirent sur le même point d’appui et une zone large du tissu permet 
d’éviter les plis et de ce fait, de tremper dans la solution.


**PROTOTYPE FINAL**

Suite à différents tests et prototypes réalisés, nous avons déterminé une version “finale” de la housse de protection, 
qui nous semble la plus adaptée, plus aboutie et qui s’adapte le mieux aux différentes utilisations des bacs 
de la chambre de fermentation mobile.

Elle reprend globalement les principes déjà existants sur les housses à notre disposition, mais de manière améliorée. 
Au lieu d'utiliser du plastique qui pourrait fondre au lavage, nous avons utilisé des lanières de sac à dos que nous avons renforcé :

![un côté ouvert](IMG_0317.jpg "un côté ouvert")
![deux côtés ouverts](IMG_0340.jpg "deux côtés ouverts")

Concernant la fermeture éclair, nous avons décidé d’en utiliser deux, qui viendront se superposer sur les rebords du bacs. 
Elles auront une fonctionnalité double, permettant d’ouvrir ainsi que de fermer la housse, 
mais également de renforcer la housse lorsqu’elles viendront frotter sur les glissières de la chambre de fermentation.

![les fermetures en guise de glissières](IMG_0343.jpg "les fermetures en guise de glissières")

Si nous avons choisi d’utiliser deux fermetures éclairs, 
c’est pour que l’on puisse ouvrir entièrement la housse lorsque l’on viendra mettre ou enlever le bac de celle- ci. 

![les fermetures entierement ouvertes](IMG_0339.jpg "les fermetures entieremnt ouvertes")

Le problème de l’utilisation des deux fermetures est qu'à l'intersection de celles-ci, il risque d’avoir un espace, 
invitant alors de potentiels envahisseurs à s’installer, c’est pourquoi nous avons rajouté un renfort en tissu pour éviter cela. 

![tissu protecteur](IMG_0336.jpg "tissu protecteur")

Ensuite, pour ne pas que le tissu trempe dans la solution lorsque l’on viendra ouvrir la housse, 
nous avons eu plusieurs idées et le prototype numéro 3 s'est finalement avéré être une inspiration essentielle. 
A la place de mettre des renforts en plastique dur qui ne pourrait probablement pas apprécier la machine à laver, 
nous avons décidé de mettre des lanières de tissu plus rigides, apportant ce côté plus résistant tout en respectant les autres contraintes.

![doublure en lanière de sac à dos](IMG_0305.jpg "doublure en lanière de sac à dos")

![doublure en lanière de sac à dos recouverte](IMG_0334.jpg "doublure en lanière de sac à dos recouverte")

De plus, pour le système d'élastiques, nous en avons cousus aux extremitées et sur la largeur, pour que lorsque l'on ouvrira la housse
elle sera retenue par ceux ci.

![systeme d'élastiques](IMG_0338.jpg "système d'élastiques")

Au sujet de la housse pour le tapis chauffant, nous avons décidé de mettre une ouverture aux deux extrémités pour pouvoir permettre de mettre plus facilement le tapis chauffant de n'importe quel côté.

![tapis chauffant prototype final](IMG-1626.jpg "tapis chauffant prototype final")

**TUTO D'UTILISATION**

[https://youtube.com/shorts/NWcXSHcjafM?feature=share](https://youtube.com/shorts/NWcXSHcjafM?feature=share)


**fiche matériaux :**

Tissu

Cordon élastique 

Fil 

Aiguille 

Lanières de sac à dos 

Fermetures éclairs de plus de 80 cm


**tuto housse :**

étape 1 :
Couper à l’aide du patron, le tissu de la housse

![création de la housse](IMG-1604.jpg "création de la housse")

étape 2 :
Coudre le tissu

![couture du tissu](IMG-1607.jpg "couture du tissu")

étape 3 : 
Rentrer le bac dans la housse et coudre sur l’avant les fermetures éclairs, après avoir fait cela, ouvrez les fermetures et couper le tissu placé entre les deux fermetures. Le fait d'épingler les fermetures éclairs et d'ensuite couper le tissu rend la manipulation beaucoup plus facile. 
Une fois les fermetures épinglés, il suffit tout simplement de venir coudre les fermetures en veillant à faire un petit ourlet au niveau du tissu pour que celui-ci ne vienne pas s'emmêler dans les fermetures.  

![couture de la fermeture](IMG-1615.jpg "couture de la fermeture")
![couper entre la fermeture](IMG-1616.jpg "couper entre la fermeture")

étape 4 : 
Une fois les fermetures cousues, il faut mettre le tissu à l'envers, de le mettre dans un bac et de venir épingler l'arrière de la housse permettant alors de venir complètement fermer la housse. 

