# **LA GRAVURE LASER SUR CELLULOSE DE KOMBUCHA**.
**MATÉRIEL**:

Afin de réaliser une *gravure* sur cellulose de kombucha, il faut des peaux de *cellulose* de kombucha non traitées, propres (nettoyage des moisissures au vinaigre) et les plus lisses possibles afin de créer un aplat qui permettra un résultat uniforme. 
Il y a aussi besoin d'une *machine à graver au laser*. 

![nettoyage](nettoyage.jpg "nettoyage")


**ÉTAPES:**

**1. Nature du fichier**.

La gravure au laser fonctionne avec des fichiers *vectoriels*. Il faut donc produire un motif en vecteurs en espace RVB à l'aide d’un logiciel qui gère ce type de fichier. (*pensez aux logiciels libres ex: Inkscape*).

![fichier](fichier.JPEG "fichier")


**2. Fonctions des couleurs**.

Le motif est ensuite sauvegardé en pdf, avec différents types de *couleurs* selon le type de gravure que l'on souhaite produire. En général, on choisit du noir pour graver et du rouge pour découper la matière.

![couleurs](couleurs.jpg "couleurs")


**3. Paramétrer le logiciel partie 1**.

Une fois ce fichier produit, il faut paramétrer le logiciel de découpe laser (ex: le logiciel UCP). Il faut d’abord rendre compte du *matériau* sur lequel sera gravé le motif parmi une large sélection: comme la plupart des machines ne connaissent pas la cellulose de kombucha, il faut choisir la matière qui s’en rapproche le plus, notamment le cuir. 

![matiere](matiere.jpeg "matiere")


**4. Paramétrer le logiciel partie 2**.

On définit également l’épaisseur (arrondi à la hausse). Les deux paramètres les plus importants pour la découpe laser sont la *puissance* et la *vitesse*. La puissance maximale (100%) permet la découpe, pour ce qui est de la gravure, des tests aux alentours de 20% à 40% permettent un résultat satisfaisant selon l’épaisseur et la qualité de la cellulose.

![parametre](parametre.jpeg "parametre")


**5. Lancement de la gravure**.

Une fois les paramètres appliqués, placer la cellulose sur le *nid d’abeille* en se rapportant aux règles visibles sur le logiciel et placer le curseur en conséquence. Suite à cela, on peut lancer la gravure et/ou la découpe. 

![gravure](gravure.jpeg "gravure")


**6. Recommendation**.

A noter que les motifs de grande taille nécessitent un certain temps d’utilisation de la machine. Il faut donc un moyen de *ventilation* pour des raisons de sécurité. 

![ventilation](ventilation.jpeg "ventilation")

# RENDU FINAL

![rendu](rendu.jpeg "rendu")

![decoupe](decoupe.jpeg "decoupe")

![final](final.JPEG "final")

![kombucha](kombucha.JPG "kombucha")

![tests](tests.png "tests")
