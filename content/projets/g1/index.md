# Groupe 1
**gras**
*italique* 
[titre de l'image](nom-De-limage.jpeg)

**Les usages de la cellulose : souder la kombucha **
*Traitement - cordelette – tissage – tressage – couture*

**Problématique autour du co-produit** : les producteurs de boisson de kombucha font fermenter du thé pendant une durée assez courte afin que la boisson ne soit pas trop acide. Au contact de l’air, une couche de cellulose se forme à la surface de la boisson, mais comme elle fermente pendant peu de temps, la couche de cellulose est fine. Comment créer un cuir de kombucha à partir d’une cellulose qui est très fine ? 

**Objectif** : créer un matériau résistant à partir d’une matière fragile fournie par le co-produit.

-	Pliage quand la kombucha est encore vivante
-	Torsade
-	Tissage
-	Tressage
-	Noeuds
-	Couture


**Le produit d’origine**

**Etape 1 : traitement de la kombucha**
- 1/3 huile de lin
- 1/3 cire d'abeille
- 1/3 essence de térébenthine

**Etape 2 : découpe de bandes de différentes tailles**

*Matière inutilisable, trop fine : elle ne sera pas résistante*

**Etape 3 : créer de la matière résistante à partir des bandelettes**
**LA TORSADE**
**LE TRESSAGE**
**LE TISSAGE**

**Etape 4 : réaliser des prototypes résistants à l'aide des expérimentations précédentes**
**A/ LA PELOTTE DE CORDELETTE**
Objectif : quand on fait du crochet ou du tricot, on utilise une pelote de laine avec une certaine longueur de fil afin de ne pas avoir à reprendre chaque nœud trop fréquemment. Notre objectif est donc de créer la cordelette la plus longue possible qui puisse être utilisable pour faire du crochet, du tricot etc. 
-	Découper des bandelettes de kombucha traitée (7mm de largeur)
-	Percer les extrémités pour les assembler et allonger la longueur de la corde
-	Plier les bandelettes en deux
-	Torsader les bandelettes sur elles-mêmes
Obtention d’une corde de 750cm

**B/ LA TOILE TISSEE**
Objectif : le cuir de kombucha peut être utilisé comme textile (pour réaliser quelconque objet, prêt-à-porter, décoration…). Notre objectif est donc de trouver un moyen de rendre le matériau résistant, et suite aux expérimentations nous avons choisi de le tisser.
-	Découper des bandelettes de kombucha traitée dans des morceaux de différentes couleurs (afin de rendre le motif plus évident)
-	Déterminer le pattern en fonction du nombre de bandelettes dans chaque couleur
-	tisser