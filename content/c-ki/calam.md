# Calam
{{< image_right img="calamportrait.png" text="Je partage ses activités entre mon doctorat en sociologie sur l'échange de la connaissance technique dans les ateliers mutualisés, et une recherche sur les biomatériaux à travers la culture de bactéries, de champignons et de plantes tinctoriales." >}}
