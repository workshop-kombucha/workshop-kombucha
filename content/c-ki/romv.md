# Romain
Étudiant en deuxième année de licence design, Romain s'intéresse à toutes les formes d'arts plastiques et appliqués.
Il a choisi de participer à ce workshop car il est plus intéressé par le côté scientifique et biologique (biomatériaux) en lien au design plutôt que les autres thèmes tels que l'espace public comme espace éditorial, l'objet comme script et l'enveloppement collectif.
Le développement/l'utilisation de biomatériaux est en effet un point important à traiter dans l'avenir, encore jamais vraiment vu dans la licence.